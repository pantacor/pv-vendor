#!/bin/sh

set -ex

PATH=$PATH:/usr/local/bin
export PATH

cmd=$0
mntpoint=${1:-/storage}

type=`echo $cmd | sed -e 's/.*\.//g'`

pcr_p=pcr:sha256:0,2,3,4,6,7,11

mkdir /mnt || true
mkdir /chroot || true

if [ -z "$type" ]; then
	echo "ERROR: must be called with filename ending with crypt type: tpm or boot"
	exit 1
fi

if ! [ "$type" == "tpm" -o "$type" == "boot" ]; then
	echo "ERROR: must be called with filename ending with crypt type: tpm or boot"
	echo "ERROR: must be either tpm or boot stored keyfile, not $type"
	exit 2
fi

mount /btools/crypt-toolbox.squashfs /chroot
mount -t proc proc /chroot/proc
mount -t devtmpfs dev /chroot/dev
mkdir /tmp/chroot
mount -o bind /tmp /chroot/tmp

echo "Sourcing luks secret ..."
if [ "$type" == "tpm" ]; then
	chroot /chroot rngd -f &
	while [ `cat /proc/sys/kernel/random/entropy_avail` -lt 1024 ]; do
		echo -n "Waiting for Entropy ... "
		sleep 1
	done
	chroot /chroot tpm2_startup
	chroot /chroot tpm2_unseal -c 0x81000000 -p $pcr_p -o /tmp/luks.key.bin
elif [ "$type" == "boot" ]; then
	mount LABEL=pvboot /mnt
	cp /mnt/luks.key.bin /tmp/
	umount /mnt
else
	echo "ERROR: no secret ... impossible codepath."
	exit 3
fi

echo "Finding Crypto Partition ..."
cryptodev=`chroot /chroot /bin/sh -c "blkid | grep TYPE=.crypto_LUKS  | sed -e 's/:.*//'"`

echo "Found Crypto Partition: $cryptodev"
chroot /chroot cryptsetup luksOpen -d  /tmp/luks.key.bin $cryptodev pv_cryptstorage

if [ "$type" == "tpm" ]; then
	chroot /chroot pkill -9 rngd
	sleep 2
fi
echo "Cleanup crypto tools ..."
umount /chroot/proc
umount /chroot/dev
umount /chroot/tmp
umount /chroot

echo "Mounting to $mntpoint ..."

mount /dev/mapper/pv_cryptstorage $mntpoint

echo "Happy trailing ... thanks for checkin-in"

exit 0
