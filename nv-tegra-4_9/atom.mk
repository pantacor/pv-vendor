
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := nv-tegra-4_9-firmware
LOCAL_CATEGORY_PATH := system

PRIVATE_FILE := nvidia_drivers.tbz2
PRIVATE_PROJECT := Linux_for_Tegra/nv_tegra/

NVTEGRA_DRIVERS_DOWN = $(PRIVATE_PATH)/downloads/
NVTEGRA_DRIVERS_DIR = $(NVTEGRA_DRIVERS_DOWN)$(PRIVATE_PROJECT)
NVTEGRA_DRIVERS_TARBALL = $(NVTEGRA_DRIVERS_DIR)$(PRIVATE_FILE)

$(NVTEGRA_DRIVERS_TARBALL):
	echo downloading: $(NVTEGRA_DRIVERS_TARBALL) && \
	mkdir -p $(NVTEGRA_DRIVERS_DIR) && \
		if [ -n "$$NVIDIA_DRIVERS_TARBALL_LOCAL" ]; then echo "Unpacking local tarball $$NVIDIA_DRIVERS_TARBALL_LOCAL"; tar -C $(NVTEGRA_DRIVERS_DOWN) -xjf $$NVIDIA_DRIVERS_TARBALL_LOCAL $(PRIVATE_PROJECT)$(PRIVATE_FILE); \
			else wget -qO - https://developer.nvidia.com/embedded/l4t/r32_release_v5.1/r32_release_v5.1/t210/jetson-210_linux_r32.5.1_aarch64.tbz2 | \
			tar -C $(NVTEGRA_DRIVERS_DOWN) -xj $(PRIVATE_PROJECT)$(PRIVATE_FILE); fi

$(PRIVATE_PATH)/firmware/tegra21x_xusb_firmware: $(NVTEGRA_DRIVERS_TARBALL)
	rm -rf $(PRIVATE_PATH)/firmware/*
	tar --strip-components=1 -C $(PRIVATE_PATH) -xjf $(NVTEGRA_DRIVERS_TARBALL) lib/firmware

$(PRIVATE_PATH)/skel/lib/firmware._4alchemy-firmware/tegra21x_xusb_firmware: $(NVTEGRA_DRIVERS_TARBALL)
	mkdir -p $(PRIVATE_PATH)/skel
	rm -rf $(PRIVATE_PATH)/skel/*
	tar --strip-components=0 -C $(PRIVATE_PATH)/skel -xjf $(NVTEGRA_DRIVERS_TARBALL) lib/firmware/tegra21x_xusb_firmware
	cd $(PRIVATE_PATH)/skel; mv lib/firmware lib/firmware._4alchemy-firmware
	

LOCAL_PREREQUISITES += $(PRIVATE_PATH)/firmware/tegra21x_xusb_firmware \
	$(PRIVATE_PATH)/skel/lib/firmware._4alchemy-firmware/tegra21x_xusb_firmware

include $(BUILD_CUSTOM)

